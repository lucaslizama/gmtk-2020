﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class TimeTravel : MonoBehaviour
{
    public int timeScale = 1;
    private List<Vector3> positions;
    private List<Quaternion> rotations;
    private List<Vector3> scales;
    private List<AnimatorStateInfo> animStates;
    private CharacterHorizontalMovement movement;
    private Character character;
    private CorgiController controller;
    private CharacterJump jump;
    public Transform self;
    public Animator anim;
    private bool travelling;

    private void Awake()
    {
        positions = new List<Vector3>();
        rotations = new List<Quaternion>();
        scales = new List<Vector3>();
        animStates = new List<AnimatorStateInfo>();
        movement = GetComponent<CharacterHorizontalMovement>();
        character = GetComponent<Character>();
        controller = GetComponent<CorgiController>();
        jump = GetComponent<CharacterJump>();
        self = transform;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.C))
        {
            travelling = false;
            controller.enabled = true;
            movement.AbilityPermitted = true;
            character.enabled = true;
            jump.enabled = true;
            movement.enabled = true;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            travelling = true;
            controller.enabled = false;
            movement.AbilityPermitted = false;
            character.enabled = false;
            movement.enabled = false;
            jump.enabled = false;
        }
    }

    private void FixedUpdate()
    {
        if (travelling && positions.Count > 0)
        {
            self.position = positions.Last();
            self.rotation = rotations.Last();
            self.localScale = scales.Last();
            anim.Play(animStates.Last().fullPathHash, 0, animStates.Last().normalizedTime);

            for (int i = 0; i < timeScale; i++)
            {
                if (positions.Count == 0) break;
                positions.RemoveAt(positions.Count - 1);
                rotations.RemoveAt(rotations.Count - 1);
                scales.RemoveAt(scales.Count - 1);
                animStates.RemoveAt(animStates.Count - 1);
            }
        }

        if (!travelling)
        {
            if (positions.Count == 600)
            {
                positions.RemoveAt(0);
                rotations.RemoveAt(0);
                scales.RemoveAt(0);
                animStates.RemoveAt(0);
            }

            positions.Add(self.position);
            rotations.Add(self.rotation);
            scales.Add(self.localScale);
            animStates.Add(anim.GetCurrentAnimatorStateInfo(0));
        }
    }
}
