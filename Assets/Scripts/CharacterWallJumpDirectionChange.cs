﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using System;

[AddComponentMenu("Corgi Engine/Character/Abilities/Character Wall Jump Direction Change")]
public class CharacterWallJumpDirectionChange : CharacterAbility
{
    /// This method is only used to display a helpbox text
    /// at the beginning of the ability's inspector
    public override string HelpBoxText() { return "Ability to change direction when walljumping"; }

    [Header("Direction")]
    public bool changeDirectionOnWalljump = true;

    private CharacterWalljump walljump;
    private float currentDirection;

    // Animation parameters
    protected const string _todoParameterName = "TODO";
    protected int _todoAnimationParameter;

    /// <summary>
    /// Here you should initialize our parameters
    /// </summary>
    protected override void Initialization()
    {
        base.Initialization();

        walljump = GetComponent<CharacterWalljump>();
        walljump.OnWallJump += OnWallJump;
    }

    /// <summary>
    /// Every frame, we check if we're crouched and if we still should be
    /// </summary>
    public override void ProcessAbility()
    {
        base.ProcessAbility();
    }

    /// <summary>
    /// Called at the start of the ability's cycle, this is where you'll check for input
    /// </summary>
    protected override void HandleInput()
    {
    }

    private void OnWallJump()
    {
        if (changeDirectionOnWalljump)
        {
            _character.Flip(true);
        }
    }

    /// <summary>
    /// Adds required animator parameters to the animator parameters list if they exist
    /// </summary>
    protected override void InitializeAnimatorParameters()
    {
        RegisterAnimatorParameter(_todoParameterName, AnimatorControllerParameterType.Bool, out _todoAnimationParameter);
    }

    /// <summary>
    /// At the end of the ability's cycle,
    /// we send our current crouching and crawling states to the animator
    /// </summary>
    public override void UpdateAnimator()
    {
        MMAnimatorExtensions.UpdateAnimatorBool(_animator, _todoAnimationParameter, (_movement.CurrentState == CharacterStates.MovementStates.Crouching), _character._animatorParameters);
    }
}