﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.CorgiEngine;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ScreenVFXRandomizer : PickableItem
{
    [Header("VFX Randomizer Pickup")]
    public PostProcessProfile[] profiles;
    public bool restoreRotation;
    private PostProcessVolume volume;
    private Transform mainCamera;

    private void Awake()
    {
        volume = GameObject.FindGameObjectWithTag("PostProcessVolume").GetComponent<PostProcessVolume>();
        mainCamera = Camera.main.transform;
    }
    protected override void Pick()
    {
        SelectRandomVFX();
        RandomRotateScreen();
    }

    private void RandomRotateScreen()
    {
        float z = Random.Range(0f, 360f);
        Vector3 rotation = new Vector3(0f, 0f, z);
        mainCamera.rotation = Quaternion.Euler(restoreRotation ? Vector3.zero : rotation);
    }

    private void SelectRandomVFX()
    {
        if (profiles.Length == 1)
        {
            volume.profile = profiles[0];
            return;
        }

        int index = 0;
        do
        {
            index = Random.Range(0, profiles.Length);
        } while (volume.profile.name == profiles[index].name);
        volume.profile = profiles[index];
    }
}
